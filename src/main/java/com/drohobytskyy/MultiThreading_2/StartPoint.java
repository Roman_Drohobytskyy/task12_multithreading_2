package com.drohobytskyy.MultiThreading_2;

import com.drohobytskyy.MultiThreading_2.Controller.Controller;
import com.drohobytskyy.MultiThreading_2.View.ConsoleView;

import java.io.IOException;

public class StartPoint {

    public static void main(String[] args) {
        {
            try {
                Controller controller = new Controller(new ConsoleView());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
