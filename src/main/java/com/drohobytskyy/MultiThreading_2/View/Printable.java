package com.drohobytskyy.MultiThreading_2.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}

