package com.drohobytskyy.MultiThreading_2.Model;

import com.drohobytskyy.MultiThreading_2.View.ConsoleView;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class LockObjects {
    public ConsoleView view;
    String obj = "";
    private final static Lock LOCK = new ReentrantLock();
    private final static Lock LOCK1 = new ReentrantLock();
    private final static Lock LOCK2 = new ReentrantLock();
    private final static Lock LOCK3 = new ReentrantLock();
    private final static ReadWriteLock MY_LOCK1 = new ReentrantReadWriteLock();
    private final static ReadWriteLock MY_LOCK2 = new ReentrantReadWriteLock();

    private void changeToFirst(String s) {
        LOCK.lock();
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "First";
        view.logger.info("String obj = " + s);
        LOCK.unlock();
    }

    private void changeToSecond(String s) {
        LOCK.lock();
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Second";
        view.logger.info("String obj = " + s);
        LOCK.unlock();
    }

    private void changeToThird(String s) {
        LOCK.lock();
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Third";
        view.logger.info("String obj = " + s);
        LOCK.unlock();
    }

    private void changeToFirstDifferent(String s) {
        LOCK1.lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "First";
        view.logger.info("String obj = " + s);
        LOCK1.unlock();

    }

    private void changeToSecondDifferent(String s) {
        LOCK2.lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Second";
        view.logger.info("String obj = " + s);
        LOCK2.unlock();

    }

    private void changeToThirdDifferent(String s) {
        LOCK3.lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Third";
        view.logger.info("String obj = " + s);
        LOCK3.lock();
    }

    private void changeToFirstMyLocker(String s) {
        MY_LOCK1.writeLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "First";
        view.logger.info("String obj = " + s);
        MY_LOCK1.writeLock().unlock();
    }

    private void changeToSecondMyLocker(String s) {
        MY_LOCK1.writeLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Second";
        view.logger.info("String obj = " + s);
        MY_LOCK1.writeLock().unlock();
    }

    private void changeToThirdMyLocker(String s) {
        MY_LOCK2.readLock().lock();
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = "Third";
        view.logger.info("String obj = " + s);
        MY_LOCK2.readLock().unlock();
    }

    public void showSynchronizedBySingleLocker() {
        new Thread(() -> changeToFirst(obj)).start();
        new Thread(() -> changeToSecond(obj)).start();
        new Thread(() -> changeToThird(obj)).start();
        try {
            sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void showSynchronizedByMultipleLockers() {
        new Thread(() -> changeToFirstDifferent(obj)).start();
        new Thread(() -> changeToSecondDifferent(obj)).start();
        new Thread(() -> changeToThirdDifferent(obj)).start();
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void showSynchronizedByMyLocker() {
        new Thread(() -> changeToFirstMyLocker(obj)).start();
        new Thread(() -> changeToSecondMyLocker(obj)).start();
        new Thread(() -> changeToThirdMyLocker(obj)).start();
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
