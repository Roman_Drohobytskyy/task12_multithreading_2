package com.drohobytskyy.MultiThreading_2.Controller;

import com.drohobytskyy.MultiThreading_2.Model.LockObjects;
import com.drohobytskyy.MultiThreading_2.View.ConsoleView;

import java.io.IOException;

public class Controller {
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    private ConsoleView view;
    LockObjects lockObjects;

    public Controller(ConsoleView view) throws IOException {
        this.view = view;
        view.controller = this;
        lockObjects = new LockObjects();
        lockObjects.view = view;
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void showSynchronizedBySingleLocker() {
        view.logger.warn(DIVIDER);
     lockObjects.showSynchronizedBySingleLocker();
    }

    public void showSynchronizedByMultipleLockers() {
        view.logger.warn(DIVIDER);
        lockObjects.showSynchronizedByMultipleLockers();
    }

    public void showSynchronizedByMyLocker() {
        view.logger.warn(DIVIDER);
        lockObjects.showSynchronizedByMyLocker();
    }
}
